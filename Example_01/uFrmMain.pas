unit uFrmMain;

interface

uses
   System.SysUtils,
   System.Types,
   System.UITypes,
   System.Classes,
   System.Variants,
   FMX.Types,
   FMX.Controls,
   FMX.Forms,
   FMX.Graphics,
   FMX.Dialogs,
   FMX.Layouts,
   FMX.Edit,
   FMX.StdCtrls,
   FMX.Controls.Presentation;

type
   TFrmMain = class(TForm)
      tlbMain: TToolBar;
      lblTitle: TLabel;
      lblEmail: TLabel;
      edtEmail: TEdit;
      lytEmail: TLayout;
      lytFullname: TLayout;
      lblFullname: TLabel;
      edtFullname: TEdit;
      lytAddress: TLayout;
      lblAddress: TLabel;
      edtAddress: TEdit;
      edtZipbox: TEdit;
      lytPhone: TLayout;
      lblPhone: TLabel;
      edtPhone: TEdit;
      lytSite: TLayout;
      lblSite: TLabel;
      edtSite: TEdit;
      procedure FormCreate(Sender: TObject);
   private
      { Private declarations }
   public
      { Public declarations }
   end;

var
   FrmMain: TFrmMain;

implementation

{$R *.fmx}

procedure TFrmMain.FormCreate(Sender: TObject);
begin
   {$IFDEF MSWINDOWS}
   Self.Top := 16;
   Self.Left := 16;
   Self.Height := 480;
   Self.Width := 320;

   lblTitle.Text := 'Windows with Delphi';
   {$ENDIF}
   //
   {$IF DEFINED(iOS)}
   lblTitle.Text := 'iOS (Apple) with Delphi';
   {$ENDIF}
   //
   {$IF DEFINED(ANDROID)}
   lblTitle.Text := 'Android (Google) with Delphi';
   {$ENDIF}
end;

end.
