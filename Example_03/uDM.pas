unit uDM;

interface

uses
   System.SysUtils,
   System.Classes,
   FireDAC.Stan.Intf,
   FireDAC.Stan.Option,
   FireDAC.Stan.Error,
   FireDAC.UI.Intf,
   FireDAC.Phys.Intf,
   FireDAC.Stan.Def,
   FireDAC.Stan.Pool,
   FireDAC.Stan.Async,
   FireDAC.Phys,
   FireDAC.Phys.SQLite,
   FireDAC.Phys.SQLiteDef,
   FireDAC.Stan.ExprFuncs,
   FireDAC.FMXUI.Wait,
   FireDAC.Stan.Param,
   FireDAC.DatS,
   FireDAC.DApt.Intf,
   FireDAC.DApt,
   Data.DB,
   FireDAC.Comp.DataSet,
   FireDAC.Comp.Client,
   FireDAC.Comp.UI;

type
   TDM = class(TDataModule)
      EmployeesConnection: TFDConnection;
      EmployeeTable: TFDQuery;
      ItemTable: TFDQuery;
      FDPhysSQLiteDriverLink: TFDPhysSQLiteDriverLink;
      FDGUIxWaitCursor: TFDGUIxWaitCursor;
      EmployeeTableID: TIntegerField;
      EmployeeTableName: TWideStringField;
      EmployeeTableDepartment: TWideStringField;
      procedure EmployeesConnectionBeforeConnect(Sender: TObject);
   private
      { Private declarations }
   public
      { Public declarations }
   end;

var
   DM: TDM;

implementation

{ %CLASSGROUP 'FMX.Controls.TControl' }

uses
   System.IOUtils;

{$R *.dfm}

procedure TDM.EmployeesConnectionBeforeConnect(Sender: TObject);
begin
   EmployeesConnection.ConnectionDefName := EmptyStr;
   EmployeesConnection.DriverName := 'SQLite';
   EmployeesConnection.LoginPrompt := False;

   {$IF DEFINED(iOS) or DEFINED(ANDROID)}
   EmployeesConnection.Params.Values['Database'] := //
      TPath.Combine(TPath.GetDocumentsPath, 'Employees.s3db');
   {$ENDIF}
   //
   {$IFDEF MSWINDOWS}
   EmployeesConnection.Params.Values['Database'] := //
      TPath.Combine('C:\Users\User\Documents\Embarcadero\Studio\Projects\Git\Example_03', 'Employees.s3db');
   {$ENDIF}
end;

end.
