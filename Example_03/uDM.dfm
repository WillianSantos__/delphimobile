object DM: TDM
  OldCreateOrder = False
  Height = 370
  Width = 405
  object EmployeesConnection: TFDConnection
    Params.Strings = (
      'ConnectionDef=Employees')
    LoginPrompt = False
    BeforeConnect = EmployeesConnectionBeforeConnect
    Left = 56
    Top = 104
  end
  object EmployeeTable: TFDQuery
    Connection = EmployeesConnection
    SQL.Strings = (
      'SELECT '
      'ID'
      ',(CAST(Name as VARCHAR(40))) as Name'
      ',(CAST(Department as VARCHAR(20))) as Department'
      'FROM Employee')
    Left = 56
    Top = 152
    object EmployeeTableID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object EmployeeTableName: TWideStringField
      AutoGenerateValue = arDefault
      FieldName = 'Name'
      Origin = 'Name'
      ProviderFlags = []
      ReadOnly = True
      Size = 32767
    end
    object EmployeeTableDepartment: TWideStringField
      AutoGenerateValue = arDefault
      FieldName = 'Department'
      Origin = 'Department'
      ProviderFlags = []
      ReadOnly = True
      Size = 32767
    end
  end
  object ItemTable: TFDQuery
    Connection = EmployeesConnection
    SQL.Strings = (
      'SELECT * FROM Item')
    Left = 56
    Top = 200
  end
  object FDPhysSQLiteDriverLink: TFDPhysSQLiteDriverLink
    Left = 56
    Top = 8
  end
  object FDGUIxWaitCursor: TFDGUIxWaitCursor
    Provider = 'FMX'
    Left = 56
    Top = 56
  end
end
