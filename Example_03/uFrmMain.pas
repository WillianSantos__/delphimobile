unit uFrmMain;

interface

uses
   System.SysUtils,
   System.Types,
   System.UITypes,
   System.Classes,
   System.Variants,
   FMX.Types,
   FMX.Controls,
   FMX.Forms,
   FMX.Graphics,
   FMX.Dialogs,
   FMX.StdCtrls,
   FMX.Controls.Presentation,
   FMX.ListView.Types,
   FMX.ListView.Appearances,
   FMX.ListView.Adapters.Base,
   FMX.ListView,
   FMX.Objects;

type
   TFrmMain = class(TForm)
      tlbMain: TToolBar;
      lblTitle: TLabel;
      lstvMain: TListView;
      btnOpen: TButton;
      Image1: TImage;
      Image2: TImage;
      Image3: TImage;
      Image4: TImage;
      Image5: TImage;

      procedure btnOpenClick(Sender: TObject);
      procedure FormCreate(Sender: TObject);
      procedure lstvMainItemClick(const Sender: TObject; const AItem: TListViewItem);
   private
      { Private declarations }
   public
      { Public declarations }
   end;

var
   FrmMain: TFrmMain;

implementation

uses
   uDM;

{$R *.fmx}

procedure TFrmMain.btnOpenClick(Sender: TObject);
var
   LItem: TListViewItem;
begin
   lstvMain.Items.Clear;

   DM.EmployeeTable.Close;
   DM.EmployeeTable.Open;

   if not DM.EmployeeTable.IsEmpty then
   begin
      lstvMain.SearchVisible := True;

      DM.EmployeeTable.First;

      while not DM.EmployeeTable.Eof do
      begin
         LItem := lstvMain.Items.Add;

         LItem.Tag := DM.EmployeeTable.FindField('ID').AsInteger;
         LItem.Text := DM.EmployeeTable.FindField('Name').AsString;
         LItem.Detail := DM.EmployeeTable.FindField('Department').AsString;

         LItem.BeginUpdate;
         {$REGION 'Image'}
         if (DM.EmployeeTable.RecNo mod 2 = 0) then
            LItem.Bitmap.Assign(Image1.Bitmap)
         else
            LItem.Bitmap.Assign(Image2.Bitmap);

         if (DM.EmployeeTable.RecNo mod 3 = 0) then
            LItem.Bitmap.Assign(Image3.Bitmap);

         if (DM.EmployeeTable.RecNo mod 4 = 0) then
            LItem.Bitmap.Assign(Image4.Bitmap);

         if (DM.EmployeeTable.RecNo mod 5 = 0) then
            LItem.Bitmap.Assign(Image5.Bitmap);
         {$ENDREGION}
         LItem.EndUpdate;

         DM.EmployeeTable.Next;
      end;
   end;
end;

procedure TFrmMain.FormCreate(Sender: TObject);
begin
   Image1.Visible := False;
   Image2.Visible := False;
   Image3.Visible := False;
   Image4.Visible := False;
   Image5.Visible := False;

   lstvMain.AlternatingColors := True;
   lstvMain.AutoTapScroll := True;
   lstvMain.NativeOptions := [ //
      TListViewNativeOption.Grouped //
      , TListViewNativeOption.Indexed //
      , TListViewNativeOption.Styled //
      ];
   lstvMain.PullRefreshWait := True;
   lstvMain.PullToRefresh := True;
   lstvMain.SelectionCrossfade := True;

end;

procedure TFrmMain.lstvMainItemClick(const Sender: TObject; const AItem: TListViewItem);
begin
   ShowMessage( //
      'ID: ' + AItem.Tag.ToString + slinebreak + //
      'Name: ' + AItem.Text + slinebreak + //
      'Dept: ' + AItem.Detail //
      );
end;

end.
