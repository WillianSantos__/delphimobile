unit uFrmMain;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.StdCtrls,
  FMX.Controls.Presentation, FMX.Edit, FMX.Layouts, FMX.TabControl, FMX.Objects;

type
  TFrmMain = class(TForm)
    tlbMain: TToolBar;
    lblTitle: TLabel;
    tbcMain: TTabControl;
    tbiLogin: TTabItem;
    tbiDept: TTabItem;
    lytEmail: TLayout;
    lblEmail: TLabel;
    edtEmail: TEdit;
    lytPassword: TLayout;
    lblPassword: TLabel;
    edtPassword: TEdit;
    imgLogo: TImage;
    lytBtnLogin: TLayout;
    btnLogin: TButton;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmMain: TFrmMain;

implementation

{$R *.fmx}

end.
